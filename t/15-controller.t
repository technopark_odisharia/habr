#!/usr/bin/perl

use utf8;
use warnings;
use strict;

use Test::More tests => 12;
use YAML::XS 'LoadFile';
use FindBin '$Bin';

use Local::Habr::Controller;
use Local::Habr::Database;      # for direct requests to the DB
use Local::Habr::Memcached;     # for direct requests to the Memcached

sub get_user_by_name_test {
    my $name_array_ref = shift;
    my $controller = shift;
    my $db = shift;
    my $memcached = shift;

    my $name = $name_array_ref->[0];
    my $karma = 23.2;
    my $rating = 3.4;

    # Cleaning before tests
    my $rs = $db->resultset('User')->find({ name => $name });
    $rs->delete() if (defined $rs);
    $memcached->delete("user:$name");

    my $result_ref = $controller->get_user_by_name($name_array_ref);
    is_deeply(
        $result_ref,
        [
            {
                name    => $name,
                karma   => $karma,
                rating  => $rating
            }
        ],
        "HTTP request is OK"
    );

    $result_ref = $controller->get_user_by_name($name_array_ref);
    is_deeply(
        $result_ref,
        [
            {
                name    => $name,
                karma   => $karma,
                rating  => $rating
            }
        ],
        "MySQL search is OK"
    );

    $result_ref = $controller->get_user_by_name($name_array_ref);
    is_deeply(
        $result_ref,
        [
            {
                name    => $name,
                karma   => $karma,
                rating  => $rating
            }
        ],
        "Memcached search is OK"
    );

    # Cleaning after tests
    ok(
        $db
            ->resultset('User')
            ->find({
                name => $name
            })
            ->delete(),
        "Deleting from DB was successful"
    );

    ok(
        $memcached->delete("user:$name"),
        "Deleting from Memcached was successful"
    )
}

sub get_user_by_name_array_test {
    my $name_array_ref = shift;
    my $controller = shift;
    my $db = shift;
    my $memcached = shift;

    my $name_1 = $name_array_ref->[0];
    my $karma_1 = 5.7;
    my $rating_1 = 17.9;

    my $name_2 = $name_array_ref->[1];
    my $karma_2 = 9.2;
    my $rating_2 = 71.2;

    # Cleaning before tests
    foreach my $name ( @{$name_array_ref} ) {
        my $rs = $db->resultset('User')->find({ name => $name });
        $rs->delete() if (defined $rs);
        $memcached->delete("user:$name")
    }

    my $result_ref = $controller->get_user_by_name($name_array_ref);
    is_deeply(
        $result_ref,
        [
            {
                name    => $name_1,
                karma   => $karma_1,
                rating  => $rating_1
            },
            {
                name    => $name_2,
                karma   => $karma_2,
                rating  => $rating_2
            }
        ],
        "HTTP request is OK"
    );

    $result_ref = $controller->get_user_by_name($name_array_ref);
    is_deeply(
        $result_ref,
        [
            {
                name    => $name_1,
                karma   => $karma_1,
                rating  => $rating_1
            },
            {
                name    => $name_2,
                karma   => $karma_2,
                rating  => $rating_2
            }
        ],
        "MySQL search is OK"
    );

    $result_ref = $controller->get_user_by_name($name_array_ref);
    is_deeply(
        $result_ref,
        [
            {
                name    => $name_1,
                karma   => $karma_1,
                rating  => $rating_1
            },
            {
                name    => $name_2,
                karma   => $karma_2,
                rating  => $rating_2
            }
        ],
        "Memcached search is OK"
    );

    # Cleaning after tests
    foreach my $name ( @{$name_array_ref} ) {
        ok(
            $db
                ->resultset('User')
                ->find({
                    name => $name
                })
                ->delete(),
            "Deleting from DB was successful"
        );

        ok(
            $memcached->delete("user:$name"),
            "Deleting from Memcached was successful"
        )
    }
}

my $config_path = "$Bin/../config/config.yaml";
my $config = LoadFile($config_path);

my $controller = Local::Habr::Controller->new($config);
my $db = Local::Habr::Database->instance();
my $memcached = Local::Habr::Memcached->instance();

get_user_by_name_test([ qw/ lair / ], $controller, $db, $memcached);
get_user_by_name_array_test([ qw/ sand14 BoryaMogila / ], $controller, $db, $memcached);

done_testing();

1;
