#!/usr/bin/perl
use warnings;
use strict;

use Test::More tests => 5;

use Local::Habr::Database;

sub user_table_test {
    my $db = shift;

    ok $db->resultset('User')->create({
        name => "Test!",
        karma => 123.2,
        rating => 123.3
    }),
        "Adding a row to the database seems to work.";

    is $db->resultset('User')->find({
        name => "Test!"
    })->name, "Test!",
        "Looking up a row from the database seems to work.";

    ok $db->resultset('User')->find({
        name => "Test!"
    })->delete,
        "Deleting a row from the database seems to work.";

    is $db->resultset('User')->find({
        name => "Test!"
    }), undef,
        "It seems that a deleted row is actually deleted.";
}

ok my $db = Local::Habr::Database->instance();

user_table_test($db);

done_testing();
