#!/usr/bin/perl

use utf8;
use warnings;
use strict;

use Test::More tests => 10;

use Local::Habr::Extractor;

sub extract_user {
    my $params = shift;

    my $extractor = Local::Habr::Extractor->new($params);
    my $data = $extractor->get_info_user();

    is (
        $data->{ name },
        "shtushkutush",
        "Extracting username from user page seems to work"
    );

    is (
        $data->{ karma },
        "24,0",
        "Extracting karma from user page seems to work"
    );

    is (
        $data->{ rating },
        "0,0",
        "Extracting rating from user page seems to work"
    );
}

sub extract_post {
    my $params = shift;

    my $extractor = Local::Habr::Extractor->new($params);
    my $data = $extractor->get_info_post();

    is (
        $data->{ title },
        "Gogland: Новая Go IDE от JetBrains",
        "Extracting title from post page works!"
    );

    is (
        $data->{ rating },
        "+62",
        "Extracting rating of post from post page works!"
    );

    is (
        $data->{ viewcount },
        "21,1k",
        "Extracting number of views from post page works!"
    );

    is (
        $data->{ favourite },
        "45",
        "Extracting number of favourites from post page works!"
    );
}

sub extract_user_from_post {
    my $params = shift;
    my $extractor = Local::Habr::Extractor->new($params);
    my $name = $extractor->get_name_from_post();

    is (
        $name,
        "VahMaster",
        "Extracting author from post in blog works!"
    )
}

sub extract_user_from_post_corporative_blog {
    my $params = shift;
    my $extractor = Local::Habr::Extractor->new($params);
    my $name = $extractor->get_name_from_post();

    is (
        $name,
        "andreycheptsov",
        "Extracting author from post in corpo-blog works!"
    )
}

sub extract_list_commenters {
    my $params = shift;
    my $extractor = Local::Habr::Extractor->new($params);
    my $commenters_array_ref = $extractor->get_list_commenters();

    my $hash_commenters_ref = { map { $_ => 1 } @{$commenters_array_ref} };

    is_deeply (
        {
            "bye" => 1,
            "formazon" => 1,
            "Severny" => 1,
            "Zaandr" => 1,
            "rossomachin" => 1,
            "den_rad" => 1
        },
        $hash_commenters_ref,
        "Get commenters from post"
    )

}

extract_user({ host => "geektimes.ru", type => "users", identity => "shtushkutush" });
extract_post({ host => "habrahabr.ru", type => "post", identity => "317716" });

extract_user_from_post({ host => "habrahabr.ru", type => "post", identity => "320412" });
extract_user_from_post_corporative_blog({ host => "habrahabr.ru", type => "post", identity => "317716" });

extract_list_commenters({ host => "habrahabr.ru", type => "post", identity => "46746" });

done_testing();
