#!/usr/bin/perl
use warnings;
use strict;

use Test::More tests => 7;
use FindBin '$Bin';
use YAML::XS 'LoadFile';

use Local::Habr::Memcached;

my $memcached;
my $config;

sub connect_to_memcached {
    is (Local::Habr::Memcached->has_instance(), undef,
        "Memcached class hasn't instance");

    my $config_path = "$Bin/../config/config.yaml";
    $config = LoadFile($config_path)->{ memcached };

    ok ($config,
        "Config is loaded");

    ok ($memcached = Local::Habr::Memcached->instance($config),
        "Memcached class has instance");

    ok ($memcached->set(
        'test_memcached',
        100,
        $config->{ exptime }
    ),
        "Adding to Memcached seems to work");

    is $memcached->get(
        'test_memcached'
    ), 100,
        "Extracting from Memcached seems to work";

    ok $memcached->delete(
        'test_memcached'
    ),
        "Deleting from Memcached seems to work";

    is $memcached->get(
        'test_memcached'
    ), undef,
        "Value has been deleted from Memcached";
}

connect_to_memcached();

done_testing();
