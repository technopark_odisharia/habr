CREATE TABLE IF NOT EXISTS users (
    id            SERIAL            NOT NULL  PRIMARY KEY,
    name          VARCHAR(128)      NOT NULL  UNIQUE,
    karma         FLOAT(12,1)       NOT NULL  DEFAULT '0.0',
    rating        FLOAT(12,1)       NOT NULL  DEFAULT '0.0'
) ENGINE = INNODB CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS posts (
    id            SERIAL            NOT NULL  PRIMARY KEY,
    post_id       BIGINT UNSIGNED   NOT NULL  UNIQUE,
    author_name   VARCHAR(128)      NOT NULL,
    title         VARCHAR(256)      NOT NULL,
    rating        FLOAT(12,1)       NOT NULL  DEFAULT '0.0',
    viewcount     FLOAT(12,1)       NOT NULL  DEFAULT '0.0',
    favourite     FLOAT(12,1)       NOT NULL  DEFAULT '0.0',

    CONSTRAINT post_fk_user FOREIGN KEY
      (author_name)
      REFERENCES users(name)
      ON DELETE CASCADE
      ON UPDATE CASCADE
) ENGINE = INNODB CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS comments (
    id            SERIAL            NOT NULL    UNIQUE  PRIMARY KEY,
    author_name   VARCHAR(128)      NOT NULL,
    post_id       BIGINT UNSIGNED   NOT NULL,

    CONSTRAINT comment_fk_user FOREIGN KEY
      (author_name)
      REFERENCES users(name)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
    CONSTRAINT comment_fk_post FOREIGN KEY
      (post_id)
      REFERENCES posts(post_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
) ENGINE = INNODB CHARACTER SET utf8;
