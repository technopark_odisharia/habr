USE habrcrawler;

DROP TABLE IF EXISTS comments;
DROP TABLE IF EXISTS posts;
DROP TABLE IF EXISTS users;

SOURCE script/mysql/create_tables.sql
