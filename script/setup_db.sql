CREATE DATABASE IF NOT EXISTS habrcrawler;

CREATE USER IF NOT EXISTS 'habr_crawler'@'localhost' IDENTIFIED BY 'habrahabr';
GRANT ALL ON habrcrawler.* TO 'habr_crawler'@'localhost';
FLUSH PRIVILEGES;

USE habrcrawler;

SOURCE script/mysql/create_tables.sql;
