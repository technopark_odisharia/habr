package Local::Habr::Extractor;

use strict;
use warnings;

use Mouse;
use List::MoreUtils qw(uniq);
use Web::Query;

use DDP;

has host        => ( required => 1, is => 'ro' );
has type        => ( required => 1, is => 'ro', writer => "_type" );
has identity    => ( required => 1, is => 'ro', writer => "_identity" );
has data        => ( required => 0, is => 'ro', writer => "_data" );

sub BUILD {
    my $self = shift;
    my $params = shift;

    my $address = $self->_string_url();

    $self->{ data } = Web::Query->new_from_url($address);

    die "Cannot complete GET request, aborting..."
        unless (defined $self->{ data });
}

sub _string_url {
    my $self = shift;
    my $address = "https://" . $self->host() . "/"
        . $self->type() . "/" . $self->identity();

    return $address
}

sub get_info_user {
    my $self = shift;

    my $profile = $self
        ->data()
        ->find('.profile-header');
    return undef unless (defined $profile);

    my $username = $profile
        ->find('.author-info__nickname')
        ->contents()
        ->as_html();
    substr($username, 0, 1) = "" if (defined $username);

    my $karma = $profile
        ->find('.voting-wjt__counter-score')
        ->contents()
        ->as_html();

    my $rating = $profile
        ->find('.statistic__value')
        ->contents()
        ->as_html();

    return {
        name        => $username,
        karma       => $karma,
        rating      => $rating
    }
}

sub get_name_from_post {
    my $self = shift;

    my $name = $self->get_info_user()->{ name };

    if (not defined $name) {
        $name = $self
            ->data()
            ->find('.company_post')
            ->find('.post-type__value_author')
            ->contents()
            ->as_html();
        substr($name, 0, 1) = "";
    }

    return $name
}

sub get_info_post {
    my $self = shift;

    my $post_content = $self
        ->data()
        ->find('.column-wrapper');
    return undef unless (defined $post_content);

    my $title = $post_content
        ->find('.post__title')
        ->find('span')
        ->not('.post__title-arrow')
        ->contents()
        ->as_html();

    my $rating = $post_content
        ->find('.postinfo-panel')
        ->find('.voting-wjt__counter-score')
        ->contents()
        ->as_html();

    my $viewcount = $post_content
        ->find('.postinfo-panel')
        ->find('.views-count_post')
        ->contents()
        ->as_html();

    my $favourite = $post_content
        ->find('.postinfo-panel')
        ->find('.favorite-wjt__counter')
        ->contents()
        ->as_html();

    return {
        title       => $title,
        rating      => $rating,
        viewcount   => $viewcount,
        favourite   => $favourite
    }
}

sub get_list_commenters {
    my $self = shift;

    my @commenters = $self
        ->data()
        ->find('#comments-list')
        ->find('.comment_body')
        ->find('.comment-item__username')
        ->contents()
        ->as_html();

    return [ uniq(@commenters) ]
}

1;
