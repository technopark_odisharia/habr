use utf8;
package Local::Habr::Database::Scheme;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-01-18 13:25:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:PsKCJPFcqgHWPQGEO7AgyQ

use FindBin '$Bin';

sub filter_loaded_credentials {
    my ( $class, $loaded_credentials, $connect_args ) = @_;

    # Getting some configuration from hash in config.yaml and changing
    # structure of result hash

    $loaded_credentials->{ dsn } = sprintf("DBI:mysql:database=%s;host=%s;port=%u",
        $loaded_credentials->{ db_name },
        $loaded_credentials->{ host },
        $loaded_credentials->{ port }
    );
    $loaded_credentials->{ user } = $loaded_credentials->{ username };
    return $loaded_credentials;
}

__PACKAGE__->load_components('Schema::Config');
__PACKAGE__->config_paths([("$Bin/../config/config")]);

1;
