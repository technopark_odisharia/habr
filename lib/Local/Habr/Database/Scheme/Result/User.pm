use utf8;
package Local::Habr::Database::Scheme::Result::User;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Local::Habr::Database::Scheme::Result::User

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<users>

=cut

__PACKAGE__->table("users");

=head1 ACCESSORS

=head2 id

  data_type: 'bigint'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 128

=head2 karma

  data_type: 'float'
  default_value: 0.0
  is_nullable: 0
  size: [12,1]

=head2 rating

  data_type: 'float'
  default_value: 0.0
  is_nullable: 0
  size: [12,1]

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "bigint",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 128 },
  "karma",
  {
    data_type => "float",
    default_value => "0.0",
    is_nullable => 0,
    size => [12, 1],
  },
  "rating",
  {
    data_type => "float",
    default_value => "0.0",
    is_nullable => 0,
    size => [12, 1],
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<name>

=over 4

=item * L</name>

=back

=cut

__PACKAGE__->add_unique_constraint("name", ["name"]);

=head1 RELATIONS

=head2 comments

Type: has_many

Related object: L<Local::Habr::Database::Scheme::Result::Comment>

=cut

__PACKAGE__->has_many(
  "comments",
  "Local::Habr::Database::Scheme::Result::Comment",
  { "foreign.author_name" => "self.name" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 posts

Type: has_many

Related object: L<Local::Habr::Database::Scheme::Result::Post>

=cut

__PACKAGE__->has_many(
  "posts",
  "Local::Habr::Database::Scheme::Result::Post",
  { "foreign.author_name" => "self.name" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-01-24 16:02:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:094MCOycoQ0TMMr6yOn5Cg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
