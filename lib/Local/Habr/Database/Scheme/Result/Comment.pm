use utf8;
package Local::Habr::Database::Scheme::Result::Comment;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Local::Habr::Database::Scheme::Result::Comment

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<comments>

=cut

__PACKAGE__->table("comments");

=head1 ACCESSORS

=head2 id

  data_type: 'bigint'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 author_name

  data_type: 'varchar'
  is_foreign_key: 1
  is_nullable: 0
  size: 128

=head2 post_id

  data_type: 'bigint'
  extra: {unsigned => 1}
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "bigint",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "author_name",
  { data_type => "varchar", is_foreign_key => 1, is_nullable => 0, size => 128 },
  "post_id",
  {
    data_type => "bigint",
    extra => { unsigned => 1 },
    is_foreign_key => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 author_name

Type: belongs_to

Related object: L<Local::Habr::Database::Scheme::Result::User>

=cut

__PACKAGE__->belongs_to(
  "author_name",
  "Local::Habr::Database::Scheme::Result::User",
  { name => "author_name" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 post

Type: belongs_to

Related object: L<Local::Habr::Database::Scheme::Result::Post>

=cut

__PACKAGE__->belongs_to(
  "post",
  "Local::Habr::Database::Scheme::Result::Post",
  { post_id => "post_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-01-24 16:02:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:YrSph6/llOR36tA1AFOcbA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
