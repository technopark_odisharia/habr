use utf8;
package Local::Habr::Database::Scheme::Result::Post;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Local::Habr::Database::Scheme::Result::Post

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<posts>

=cut

__PACKAGE__->table("posts");

=head1 ACCESSORS

=head2 id

  data_type: 'bigint'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 post_id

  data_type: 'bigint'
  extra: {unsigned => 1}
  is_nullable: 0

=head2 author_name

  data_type: 'varchar'
  is_foreign_key: 1
  is_nullable: 0
  size: 128

=head2 title

  data_type: 'varchar'
  is_nullable: 0
  size: 256

=head2 rating

  data_type: 'float'
  default_value: 0.0
  is_nullable: 0
  size: [12,1]

=head2 viewcount

  data_type: 'float'
  default_value: 0.0
  is_nullable: 0
  size: [12,1]

=head2 favourite

  data_type: 'float'
  default_value: 0.0
  is_nullable: 0
  size: [12,1]

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "bigint",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "post_id",
  { data_type => "bigint", extra => { unsigned => 1 }, is_nullable => 0 },
  "author_name",
  { data_type => "varchar", is_foreign_key => 1, is_nullable => 0, size => 128 },
  "title",
  { data_type => "varchar", is_nullable => 0, size => 256 },
  "rating",
  {
    data_type => "float",
    default_value => "0.0",
    is_nullable => 0,
    size => [12, 1],
  },
  "viewcount",
  {
    data_type => "float",
    default_value => "0.0",
    is_nullable => 0,
    size => [12, 1],
  },
  "favourite",
  {
    data_type => "float",
    default_value => "0.0",
    is_nullable => 0,
    size => [12, 1],
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<post_id>

=over 4

=item * L</post_id>

=back

=cut

__PACKAGE__->add_unique_constraint("post_id", ["post_id"]);

=head1 RELATIONS

=head2 author_name

Type: belongs_to

Related object: L<Local::Habr::Database::Scheme::Result::User>

=cut

__PACKAGE__->belongs_to(
  "author_name",
  "Local::Habr::Database::Scheme::Result::User",
  { name => "author_name" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 comments

Type: has_many

Related object: L<Local::Habr::Database::Scheme::Result::Comment>

=cut

__PACKAGE__->has_many(
  "comments",
  "Local::Habr::Database::Scheme::Result::Comment",
  { "foreign.post_id" => "self.post_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-01-24 16:02:33
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:5tuKmFat6YeIzWb9yLSAGA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
