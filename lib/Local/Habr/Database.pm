package Local::Habr::Database;

use v5.10;
use strict;
use warnings;

use base 'Class::Singleton';

use Local::Habr::Database::Scheme;

use DDP;

sub _new_instance {
    my $class = shift;
    my $self  = bless { }, $class;

    $self->{ database } = Local::Habr::Database::Scheme->connect(
        'db'
    ) || die "Cannot connect to database...\n";

    return $self->{ database }
}

1;
