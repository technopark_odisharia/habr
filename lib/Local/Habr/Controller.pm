package Local::Habr::Controller;

use strict;
use warnings;

use Mouse;
use DBIx::Class;
use Cache::Memcached::Fast;

use Local::Habr::Extractor;
use Local::Habr::Database;
use Local::Habr::Memcached;

use DDP;

has http_config =>  (
    required => 0,
    is => 'ro',
    builder => '_default_http_config'
);

has memcached_config => (
    required => 0,
    is => 'ro',
    default => sub {
        (
            exptime => 3600
        )
    }
);

has db          =>  ( required => 0, is => 'ro' );
has memcached   =>  ( required => 0, is => 'ro' );

sub BUILD {
    my $self = shift;
    my $config = shift;

    $self->{ http_config } = { host => $config->{ http_config }->{ host } };
    $self->{ memcached_config } = { exptime => $config->{ memcached }->{ exptime } };

    $self->{ db } = Local::Habr::Database->instance();
    $self->{ memcached } = Local::Habr::Memcached->instance($config->{ memcached });
}

sub _default_http_config {
    return {
        host => 'habrahabr.ru'
    }
}

sub _make_float {
    my $self = shift;
    my $entity = shift;

    $entity->{ karma }  =~ s/\,/\./ if $entity->{ karma };
    $entity->{ rating }  =~ s/\,/\./ if $entity->{ rating };
}

sub _db_user_handler {
    my $self = shift;
    my $name = shift;

    # Database search
    my $user = $self
        ->db()
        ->resultset('User')
        ->search({
            name => "$name"
        }, {
            columns => [qw/ name karma rating /]
        })
        ->first();

    if (not defined $user) {
        # HTTP request
        $user = Local::Habr::Extractor
            ->new(
                host        => "habrahabr.ru",
                type        => "users",
                identity    => $name
            )
            ->get_info_user();
        $self->_make_float($user);
        $self->db()->resultset('User')->create({
            name    => $user->{ name },
            karma   => $user->{ karma },
            rating  => $user->{ rating }
        })
    }

    return $user;
}

sub get_user_by_name {
    my $self = shift;
    my $name_array_ref = shift;

    my @users;

    foreach my $name (@{$name_array_ref}) {
        # Memcached search
        my $user = $self->memcached()->get("user:$name");

        if (not defined $user) {
            # Database search
            $user = $self->_db_user_handler($name);

            $self->memcached()->set(
                "user:$name",
                $user,
                $self->memcached_config()->{ exptime }
            );
        }

        push @users, $user;
    }

    return \@users
}

sub get_user_by_post {
    my $self = shift;
    my $post_array_ref = shift;

    my @users;

    foreach my $post ( @{$post_array_ref} ) {
        my $name = Local::Habr::Extractor
            ->new(
                host        => "habrahabr.ru",
                type        => "post",
                identity    => $post
            )
            ->get_name_from_post();

        # Database search
        my $user = $self->_db_user_handler($name);

        $self->memcached()->set(
            "user:$name",
            $user,
            $self->memcached_config()->{ exptime }
        );

        push @users, $user;
    }

    return \@users
}

sub get_commenters_by_post {
    my $self = shift;
    my $post_array_ref = shift;

    my @users;

    foreach my $post ( @{$post_array_ref} ) {
        my $name_array_ref = Local::Habr::Extractor
            ->new(
                host        => "habrahabr.ru",
                type        => "post",
                identity    => $post
            )
            ->get_list_commenters();

        foreach my $name ( @{$name_array_ref} ) {
            my $user = $self->_db_user_handler($name);
            push @users, $user
        }
    }

}

sub get_post_by_id {
    my $self = shift;
}

1;
