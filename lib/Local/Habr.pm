package Local::Habr;

=encoding utf8

=head1 NAME

Local::Habr - habrahabr.ru crawler

=head1 VERSION

Version 1.00

=cut
our $VERSION = '1.00';

=head1 SYNOPSIS

=cut

use strict;
use warnings;

use Mouse;
use YAML::XS 'LoadFile';
use FindBin '$Bin';

use Local::Habr::Controller;
use Local::Habr::Reporter;

use DDP;

has name        => ( is => 'ro', required => 1 );
has post        => ( is => 'ro', required => 1 );
has id          => ( is => 'ro', required => 1 );
has n           => ( is => 'ro', required => 1 );

has controller  => ( is => 'ro' );
has reporter    => ( is => 'ro' );

sub BUILD {
    my $self = shift;
    my $params_ref = shift;

    my $config_path = "$Bin/../config/config.yaml";
    my $config = LoadFile($config_path);
    $config->{ http_config }->{ host } = $params_ref->{ host };

    $self->{ name } = "undef";
    $self->{ post } = "undef";
    $self->{ id } = 1;
    $self->{ n } = 0;

    $self->{ controller } = Local::Habr::Controller->new($config)


}

1;
