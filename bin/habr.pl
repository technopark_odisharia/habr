#!/usr/bin/env perl

use strict;
use warnings;

use Getopt::Long qw(GetOptionsFromArray);
use Pod::Usage;
use List::MoreUtils qw(uniq);

use FindBin;
use lib "$FindBin::Bin/../lib/";

use Local::Habr;

use DDP;

=head1 VERSION

=cut

our $VERSION = v0.01;

my $MODE_USER = 0;
my $MODE_POST = 1;
my $MODE_COMMENT = 2;
my $MODE_SELF = 3;
my $MODE_DESERT = 4;

my %mode = (
    'user'              => $MODE_USER,
    'post'              => $MODE_POST,
    'commenters'        => $MODE_COMMENT,
    'self_commenters'   => $MODE_SELF,
    'desert_posts'      => $MODE_DESERT
);

sub show_requests {
    pod2usage(
        -exitval => 2,
        -verbose => 99,
        -sections => [ qw(SYNOPSIS REQUESTS) ]
    );
}

sub parse_args {
    my $args_ref = shift;
    my $config_ref = shift;

    GetOptionsFromArray($args_ref, $config_ref,
        '<>' => sub {
            $config_ref->{mode} = $mode{$_[0]};
        },
        'help' => sub {
            pod2usage(
                -verbose => 99,
                -sections => [ qw(SYNOPSIS REQUESTS OPTIONS) ]
            )
        },
        'version' => sub {
            pod2usage(
                -verbose => 99,
                -sections => "VERSION"
            )
        },
        'host=s',
        'name=s@',
        'post=i@',
        'id=i@',
        'n=i'
    ) || pod2usage(2);

    # if (!defined $config_ref->{mode}) {
    #     print "Request should be specified\n\n";
    #     show_requests();
    # }
    # elsif ($config_ref->{mode} == $MODE_USER) {
    #     if (!defined($config_ref->{name} // $config_ref->{post})) {
    #         print "Either '--name' or '--post' option should be specified\n\n";
    #         show_requests();
    #     }
    # }
    # elsif ($config_ref->{mode} == $MODE_COMMENT) {
    #     if (!defined $config_ref->{post}) {
    #         print "'--post' option should be specified\n\n";
    #         show_requests();
    #     }
    # }
    # elsif ($config_ref->{mode} == $MODE_POST) {
    #     if (!defined $config_ref->{id}) {
    #         print "'--id' option should be specified\n\n";
    #         show_requests();
    #     }
    # }
    # elsif ($config_ref->{mode} == $MODE_DESERT) {
    #     if (!defined $config_ref->{n}) {
    #         print "'--n' option should be specified\n\n";
    #         show_requests();
    #     }
    # }
    # else {
    #     print "Wrong request\n\n";
    #     show_requests();
    # }

    $config_ref->{host} //= 'habrahabr.ru';

    if ($config_ref->{name}) {
        @{$config_ref->{name}} = uniq split ',', join(',', @{$config_ref->{name}});
    }
    else {
        $config_ref->{name} = [];
    }

    if ($config_ref->{post}) {
        @{$config_ref->{post}} = uniq split ',', join(',', @{$config_ref->{post}});
    }
    else {
        $config_ref->{post} = [];
    }

    if ($config_ref->{id}) {
        @{$config_ref->{id}} = uniq split ',', join(',', @{$config_ref->{id}});
    }
    else {
        $config_ref->{id} = [];
    }

    $config_ref->{n} //= 0;
}

my %config;
parse_args(\@ARGV, \%config);

my $habr = Local::Habr->new(\%config);

# if ($config{mode} == $MODE_USER) {
#     $habr->mode_user
# }
# elsif ($config{mode} == $MODE_POST) {
#     $habr->mode_post
# }
# elsif ($config{mode} == $MODE_COMMENT) {
#     $habr->mode_comment
# }
# elsif ($config{mode} == $MODE_SELF) {
#     $habr->mode_self
# }
# elsif ($config{mode} == $MODE_DESERT) {
#     $habr->mode_desert
# }



1;

__END__

=head1 NAME

Habracrawler — habrahabr and geektimes crawler

=head1 SYNOPSIS

./habr REQUEST [options...]

=cut

=head1 REQUESTS

=over 0

=item All requests contain mode and option

=item REQUEST = MODE OPTION

=back

=over 4

=item B<user --name NAME>

information about user with name NAME

=item B<user --post POST_ID>

information about author of post POST_ID

=item B<commenters --post POST_ID>

information about users commented post POST_ID

=item B<post --id POST_ID>

information about post POST_ID

=item B<self_commentors>

information about every known user who commented their own posts at least one time

=item B<desert_posts --n NUMBER>

information about posts with number of commentators less or equal than given threshold

=back

=head1 OPTIONS

=over 20

=item B<--format>

output format

=item B<--refresh>

use the most trusted source

=item B<--host>

hostname (habrahabr.ru or geektimes.ru)

=cut
